-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 26. Februar 2013 um 20:57
-- Server Version: 5.5.8
-- PHP-Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `SQL_Demo`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_artikel`
--

CREATE TABLE `tbl_artikel` (
  `artikel_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Artikelname` text NOT NULL,
  `Preis` float DEFAULT NULL,
  PRIMARY KEY (`artikel_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Daten für Tabelle `tbl_artikel`
--

INSERT INTO `tbl_artikel` VALUES(1, 'Schrauben M5', 2.5);
INSERT INTO `tbl_artikel` VALUES(2, 'Muttern M%', 1.15);
INSERT INTO `tbl_artikel` VALUES(3, 'Gewindestangen M5', 5.6);
INSERT INTO `tbl_artikel` VALUES(4, 'Flügelschrauben E5', NULL);
INSERT INTO `tbl_artikel` VALUES(5, 'Gewindebohrer SM70', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_personen`
--

CREATE TABLE `tbl_personen` (
  `Person_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(40) NOT NULL DEFAULT '',
  `Vorname` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`Person_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='enth�lt die Personen' AUTO_INCREMENT=9 ;

--
-- Daten für Tabelle `tbl_personen`
--

INSERT INTO `tbl_personen` VALUES(1, 'Müller', 'Maria');
INSERT INTO `tbl_personen` VALUES(2, 'Malcom', 'Gustav');
INSERT INTO `tbl_personen` VALUES(3, 'Peronik', 'Lars');
INSERT INTO `tbl_personen` VALUES(4, 'Dolber', 'Heiko');
INSERT INTO `tbl_personen` VALUES(5, 'Knosdz', 'Jana');
INSERT INTO `tbl_personen` VALUES(6, 'Müller', 'Manfred');
INSERT INTO `tbl_personen` VALUES(7, 'Müller', 'Ruedi');
INSERT INTO `tbl_personen` VALUES(8, 'Müller', 'Chiara');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_rechnungen`
--

CREATE TABLE `tbl_rechnungen` (
  `Rechnungs_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Datum` date DEFAULT NULL,
  `Betrag` float DEFAULT NULL,
  `Person_FS` int(11) DEFAULT NULL,
  PRIMARY KEY (`Rechnungs_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='enth�lt die Rechnungen' AUTO_INCREMENT=6 ;

--
-- Daten für Tabelle `tbl_rechnungen`
--

INSERT INTO `tbl_rechnungen` VALUES(1, '2003-02-04', 588.32, 1);
INSERT INTO `tbl_rechnungen` VALUES(2, '2003-02-21', 4855.2, 1);
INSERT INTO `tbl_rechnungen` VALUES(3, '2003-03-01', 5800, 2);
INSERT INTO `tbl_rechnungen` VALUES(4, '2003-02-29', 5860.2, 3);
INSERT INTO `tbl_rechnungen` VALUES(5, '2012-12-23', 580.6, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_trans`
--

CREATE TABLE `tbl_trans` (
  `FS_Rechnung` int(11) NOT NULL DEFAULT '0',
  `FS_Artikel` int(11) NOT NULL DEFAULT '0',
  `Anzahl` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tbl_trans`
--

INSERT INTO `tbl_trans` VALUES(1, 1, 4);
INSERT INTO `tbl_trans` VALUES(2, 2, 22);
INSERT INTO `tbl_trans` VALUES(3, 3, 55);
INSERT INTO `tbl_trans` VALUES(1, 1, 2);
INSERT INTO `tbl_trans` VALUES(3, 1, 3);
INSERT INTO `tbl_trans` VALUES(1, 2, 5);
INSERT INTO `tbl_trans` VALUES(1, 3, 9);
INSERT INTO `tbl_trans` VALUES(0, 2, 5);
