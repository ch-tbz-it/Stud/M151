# M151 - Datenbanken in Web-Applikation einbinden.

[> **Modulidentifikation** ](https://www.modulbaukasten.ch/module/151/3/de-DE?title=Datenbanken-in-Web-Applikation-einbinden)

## LB1 (25%, Erklär-Dokument)
[-> Erklären Sie in diesem Dokument folgende Themen:](./erklaeren-der-themen.txt)
- 1 Design
- 2 Sicherheit
- 3 Realisierung
- 4 Change(-management)
- 5 Testing


## LB2 (25%, schriftliche Prüfung, 90 min) 
- Multi-Tier-Architektur(en)
- Design-Phasen von Applikation und Datenbanken
- DB-Entwicklungsprozess ERM/ERD, SQL, DDL, DML, DCL, Tabellen, Views
- Ausgewählte PHP-Code-Stücke (z.B. zur DB-Anbindung)

## LB3 (50%, praktisches Projekt) 
--> Verwaltungs-WebApp mit 2 verschiedenen Datenbanken. Eigenes Thema.
z.B. Webshop mit mind. 10 Artikeln in der DB mit Bestellmöglichkeit bis Rechnungstellung (ohne Bezahlmechanismus)
### Grundanforderung (bis Note 4.7)
 -  Dynamisch generierte Website (z.B. in PHP) mit 1 Datenbankanbindung programmiert.
 -  Statischer DB-Switch (hart codiert)
 
 [weitere Bedingungen für Note 4.7](./mindestbedingungen-fuer-note-4.7.txt)

#### Adminbereich
 -	Verschlüsseltes Login mit Sessionüberprüfung
 -	Benutzerverwaltung mit verschiedenen Rechten
 -	Online-Administration der Websiteinhalte
 -	Formularüberprüfungen

#### Userbereich
 -	Navigation funktionsgerecht
 -	Sortieren möglich
 -	Zeitgerechte Usability (responsive, Steuer- und Anzeigeelemente)

### Erweiterte Anforderungen (über Note 4.8)
- Mehr als eine Datenbank implementiert

 1. ) **Oracle** Express: Datenbankanbindung programmieren
 2. ) **PostgreSQL**: Datenbankanbindung mit transaktionssicheren Abfragen programmieren
 3. ) **MSSQL Express**: Datenbankanbindung programmieren
 4. ) **MySQL/MariaDB**: Innodb Tabellen mit transaktionssicheren Abfragen und MVC programmieren
 5. ) **Andere** Datenbankanbindung programmieren (NoSQL, MongoDB, ..)

- Bonus: Dynamischer DB-Switch (ohne Code-Änderung)
- Bonus: Externe Schnittstelle implementiert (Zahlungssystem einbinden, z.B. TWINT)



## Bewertungsraster
| Thema/Aufgabe                                         | (-0.3)                                                           | Note 4.0                                                     | (+0.3)                                                       |
| ----------------------------------------------------- | ---------------------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Aufgabe 1** <br/>Adminbereich samt Login und Userbereich (mit und ohne Login) realisieren | Aufgaben unvollständig. Daten wurden unvollständig oder gar nicht übernommen. Keine Benutzeridentifikation realisiert. Resultate sind für eine Anwendung unbrauchbar | 1. Verschlüsseltes Login mit Sessionüberprüfung <br/>2. Benutzerverwaltung mit verschiedenen Rechten <br/>3. Online-Administration der Websiteinhalte <br/>4. Formularüberprüfung | Datenstruktur und Datentypen wurden angelegt. Umfangreiches Benutzerkonzept erstellt und zumindest teilweise realisiert (z. B. Benutzerverwaltung über DB-Tabellen). |
| **Aufgabe 2** <br/>Datenbankserver mit transaktionssicheren Abfragen. (Oracle-Express, MSSQL-Express, MYSQL, Postgresql) | Applikation ist nicht lauffähig, Daten werden nicht vollständig angezeigt, Navigation ist nicht anwendergerecht implementiert. | Anbindung Applikation und Datenbank ist funktionsfähig. Daten der DB können administriert werden. Transaktionssichere Abfragen wurden realisiert, Navigation ist möglich. | SQL der Datenbank angepasst. Sinnvolle, grafisch ansprechende Darstellung der Daten. Daten können sortiert werden. Navigation ist übersichtlich und funktionsfähig. |
| **Aufgabe 3** <br/>MVC, 2-Tier, 3-Tier, 4-Tier        | MVC nicht oder nur teilweise angewendet                          | MVC realisiert und funktionsfähig -> "läuft"                 | MVC realisiert und vorbildlich implementiert (bis ins Detail)|
| **Aufgabe 4** <br/>Eigene Funktionen - Eigene Klassen | Keine eigenen Funktionen entwickelt                              | Mehrere eigene Funktionen entwickelt und sinnvoll eingesetzt | Umfangreiches Funktionskonzept umgesetzt                     |
| **Aufgabe 5** <br/>Anwendung testen                   | Tests nicht oder nur wenig durchgeführt, kein Testkonzept, keine Testanleitung vorhanden | Tests wurden durchgeführt und dokumentiert. Testanleitung. Gefundene Fehler sind beschrieben. | Testkonzept vorhanden und durchgeführt. Testergebnisse sind dokumentiert und nachvollziehbar |
| **Bonus 1** <br/>Läuft im Internet auf (Sub-)Domain | nur lokal | Internet mit 1 DB | Internet mit 2 DBs |
| **Bonus 2** <br/>DB-Switch | - | statisch | dynamisch | 
| **Bonus 3** <br/>Einbindung externen Schnittstelle | - | - | z.B. Zahlungssystem wie TWINT oder Datatrans eingebunden und funktioniert | 
|  |  |  |  |
